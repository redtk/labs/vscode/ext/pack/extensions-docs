# redtk.redtk-extensions-docs

## Installation

Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter.

```text
ext install redtk.redtk-extensions-docs
```

## Prepared for
Projects documentation, knowledge base.
